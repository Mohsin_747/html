let firstCard = 10;
let secondCard = 4;
let sum = firstCard + secondCard;
let cards = [firstCard,secondCard];

let message = " ";

let messageEL = document.getElementById("message-el");
let sumEl = document.getElementById("sum-el");
let cardsEl = document.getElementById("cards-el");

function renderGame()
{
    cardsEl.textContent = "Cards: ";
    for(let i=0; i<cards.length; i++)
    {
        cardsEl.textContent += cards[i] + " ";
    }
    
    sumEl.textContent = "Sum: "+ sum;

    if(sum <= 20){
        message = "Do you want to draw a new Card?";
    }
    else if(sum === 21)
    {
        message = "Congrats, You got a BlackJack!";
    }else{
        message = "I'm Sorry, Game Over";
    }
    messageEL.textContent = message;   
}
function newCard()
{
    console.log("Drawing a new card from Deck");
    let card = 7;
    sum += card;
    cards.push(card);
    renderGame();
}

function startGame()
{
    renderGame();
}

// let fear = ["newone","brown","secondelemtn"]
// console.log(fear.length)
// console.log(fear[0]);